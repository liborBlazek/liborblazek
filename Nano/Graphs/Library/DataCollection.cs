﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphs.Library
{
    /// <summary>Kolekce pro práci s časovými řadami</summary>
    public class DataCollection<T> : Collection<T>
    {
        /// <summary>Najde nejbliží hodnotu v kolekci podle času</summary>
        public DataPoint GetItem(DateTime time)
        {
            try
            {
                // hledam od konce
                foreach (T item in this.Reverse())
                {
                    DataPoint point = (DataPoint)(object)item;

                    if (point.Time < time)
                    {
                        return point;
                    }
                }
            }
            catch 
            {
                return null;
            }
            return null;
        }

        /// <summary>Poslední prvky obou kolekcí mají stejný čas</summary>
        public bool EqualEnd(DataCollection<T> second)
        {
            bool res = true;
            try
            {
                if (this.Count > 0 && second.Count > 0)
                {
                    DataPoint last = (DataPoint)(object)this.Last();
                    DataPoint last2 = (DataPoint)(object)second.Last();
                    if (last.Time != last2.Time)
                        res = false;
                }
            }
            catch
            {
                return false;
            }
            return res;
        }

        /// <summary>Odstranění všech členů s novějším datem</summary>
        public bool StripAfter(DateTime date)
        {
            bool res = true;
            // vymazani starych dat
            foreach (T item in this.Reverse())
            {
                DataPoint point = (DataPoint)(object)item;
                if (point.Time > date)
                {
                    this.Remove(item);
                }
                else 
                {
                    break;
                }
            }
            return res;
        }

        /// <summary>Odstranění všech členů se starším datem</summary>
        public bool StripBefore(DateTime date)
        {
            bool res = true;
            // vymazani starych dat
            foreach (T item in this.Reverse())
            {
                DataPoint point = (DataPoint)(object)item;
                if (point.Time < date)
                {
                    this.Remove(item);
                }
            }
            return res;
        }

        /// <summary>
        /// Setřídění prvků podle výchozího řazení
        /// </summary>
        public void Sort()
        {
            ((List<T>)Items).Sort();
        }

 
        /// <summary>
        /// Převod dat na minutova
        /// </summary>
        /// <param name="cons">Hodinova data</param>
        /// <returns>Minutova data</returns>
        public static DataCollection<DataPoint> ToMinuteData(DataCollection<DataPoint> cons)
        {
            // novy vysledek
            DataCollection<DataPoint> newCons = new DataCollection<DataPoint>();

            // prevod na minuty
            bool first = true;
            DataPoint previous = cons.First();

            foreach (DataPoint item in cons)
            {
                if (!first)
                {
                    for (int i = 0; i < 60; i++)
                    {
                        newCons.Add(new DataPoint
                        {
                            Time = previous.Time + new TimeSpan(0, i, 0),
                            Value = (i * item.Value + (60 - i) * previous.Value) / 60
                        });
                    }
                }
                first = false;
                // prechod na dalsi hodinu
                previous = item;
            }

            return newCons;
        }
    }
}
