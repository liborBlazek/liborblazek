﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphs.Library
{
    /// <summary>Reprezentace jednoho bodu v čase</summary>
    public class DataPoint : IComparable
    {
        /// <summary>Datum a čas hodnot</summary> 
        public DateTime Time { get; set; }

        /// <summary>Hlavní hodnota pro zobrazení</summary> 
        public double Value { get; set; }

        /// <summary>Porovnání podle času</summary> 
        /// <param name="second">Druhý objekt k porovnání</param>
        public int CompareTo(object second) 
        {
            return this.Time.CompareTo(((DataPoint)second).Time); 
        }

        /// <summary>Porovnání podle času</summary> 
        /// <param name="second">Druhý objekt k porovnání</param>
        public int CompareTo(DataPoint second)
        {
            return this.Time.CompareTo(second.Time);
        }
    }
}
