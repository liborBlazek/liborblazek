﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.Globalization;
using System.Windows;
using Graphs.Library;

namespace Graphs.Model
{
    /// <summary>Odhad výroby obnovitelných zdrojů</summary>
    public class GenerationOze : DataPoint
    {
        /// <summary>Součet</summary> 
        public double Sum { get { return FVE + VTE; } private set { } }
        /// <summary>FVE</summary> 
        public double FVE { get; set; }
        /// <summary>VTE</summary> 
        public double VTE { get; set; }

        /// <summary>Vrací řetězec ve formátu datum a Sum</summary> 
        public override string ToString()
        {
            return Time.ToString("d.M.yyyy HH:mm:ss") + " " + String.Format("{0:0.###}", Sum);
        }

        /// <summary>Načtení dat z internetu</summary> 
        public static DataCollection<GenerationOze> Load()
        {
            return Load(new TimeSpan(24, 0, 0), DateTime.Now);
        }

        /// <summary>Načtení dat z internetu</summary> 
        /// <param name="ts">Množství dat k načtení - do "to"</param>
        /// <param name="to">Čas posledních dat</param>
        public static DataCollection<GenerationOze> Load(TimeSpan ts, DateTime to)
        {
            DataCollection<GenerationOze> collection = new DataCollection<GenerationOze>();
            try
            {
                DateTime from = to - ts;

                // zavolání api
                CepsData.CepsData svcClient = new CepsData.CepsData();
                System.Xml.XmlNode resultNode = svcClient.GenerationOze(from, to, "MI", "AVG", "RT", "all");

                // přidání namespace
                XmlNamespaceManager manager = new XmlNamespaceManager(resultNode.OwnerDocument.NameTable);
                manager.AddNamespace("x", resultNode.NamespaceURI);
                System.Xml.XmlNode data = resultNode.SelectSingleNode("x:data", manager);

                foreach (XmlNode node in resultNode.SelectNodes("//x:item", manager))
                {
                    DateTime date = Convert.ToDateTime(node.Attributes["date"].InnerText, CultureInfo.InvariantCulture);
                    double FVE = Convert.ToDouble(node.Attributes["value1"].InnerText, CultureInfo.InvariantCulture);
                    double VTE = Convert.ToDouble(node.Attributes["value2"].InnerText, CultureInfo.InvariantCulture);

                    collection.Add(new GenerationOze
                    {
                        Time = date,
                        VTE = VTE,
                        FVE = FVE
                    });
                }

            }
            catch (Exception ex) { 
                // místo logování
                MessageBox.Show(ex.Message);
            }

            return collection;
        }

    }
}
