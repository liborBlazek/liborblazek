﻿using Graphs.Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Graphs.Model
{
    class DataModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Pravidelná aktualizace dat
        /// </summary>
        private System.Timers.Timer _timer;

        public string GenerationTitle
        {
            get
            {
                 return "Produkce";
            }
            private set { }
        }

        /// <summary>Odhad výroby obnovitelných zdrojů</summary>
        public DataCollection<GenerationOze> GenerationOzes { get; private set; }
        public string GenerationOzeTime { get; set; }

        public DataModel()
        {
            GenerationOzes = new DataCollection<GenerationOze>();

            if (_timer == null)
            {
                // interval načítání
                _timer = new System.Timers.Timer(Properties.Settings.Default.Interval);
                _timer.Elapsed += new ElapsedEventHandler(OnTimerElapsed);
                _timer.Start();
            }
            // načtení dat
            Load();
        }

        /// <summary>
        /// První načtení dat
        /// </summary>
        private void Load()
        {
            Task.Factory.StartNew(() =>
            {
                GenerationOzes = GenerationOze.Load();
                OnPropertyChanged("GenerationOzes");
            });
        }

        /// <summary>
        /// Funkce se provede pri vyprseni casovace.
        /// </summary>
        private void OnTimerElapsed(object source, ElapsedEventArgs e)
        {
            Update();
        }

        /// <summary>
        /// Načtení dalších dat
        /// </summary>
        private void Update()
        {
            DataCollection<GenerationOze> newGenerationOzes = GenerationOze.Load();

            // data se našla
            if (newGenerationOzes.Count != 0)
            {
                GenerationOzes = newGenerationOzes;
                OnPropertyChanged("GenerationOzes");
            }
        }


        #region INotifyPropertyChanged Members
        /// <summary>
        /// Veřejná událost pro vyvolání změny
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            // kontrola existence
            if (GetType().IsVisible)
            {
                if (!string.IsNullOrEmpty(propertyName) && GetType().GetProperty(propertyName) == null)
                    throw new ArgumentException("Error", "propertyName");
            }

            if (PropertyChanged != null)
            {
                string newtime = DateTime.Now.ToString("d.M.yyyy HH:mm:ss");
                switch (propertyName)
                {
                    case "GenerationOzes":
                        GenerationOzeTime = newtime;
                        OnPropertyChanged("GenerationOzeTime");
                        break;

                }
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
